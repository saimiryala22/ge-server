/* Site Constants*/
module.exports = {
	MAIL : {
		REGISTRATION: 'registration',
		FORGOT_PASSWORD : 'forgot_password',
		SURVEY_PART_MAIL : 'survey_part_mail'
	},
	ACTORS : {
		SUPERADMIN : 'super_admin',
		ADMIN : 'admin',
		TRAINER : 'trainer',
		PARTICIPANT : 'participant',
		CLIENT : 'client'
	},
	SURVEY_STATE : {
		CLIENT_DETAILS : 'client_details',
		DEMOGRAGHICS : 'demographics',
		QUESTIONNAIRE : 'questionnaire',
		PARTICIPANT : 'participant',
		RATING_SCALE : 'rating_scale',
		DASHBOARD : 'dashboard'
	},
	
};