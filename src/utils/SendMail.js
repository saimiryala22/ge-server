const path = require('path')
const templatesDir = path.resolve(__dirname, '..', 'emails')
const transporter = require('../services/transporter');
const config = require('../config')
const Email = require('email-templates');
const Constants = require('./Constants')

// User Registration Mail
exports.userRegistrationMail = async (data,mailType) => {
	var templatePath = '';
	switch(mailType){
		case Constants.MAIL.REGISTRATION:
		templatePath = templatesDir+'/registration';
		break;
		default:
	}
	const email = new Email({
	  message: {
		from: 'noreply'
	  },
	  // uncomment below to send emails in development/test env:
	  send: true,
	  transport: transporter
	});
	
	email.send({
    template: templatePath,
    message: {
      to: data.email
    },
    locals: {
      name: data.userName,
      hostname: config.hostname,
      activationKey: data.activationKey,
    }
  })
  .catch(console.error);
}