'use strict'

const User = require('../models/userMaster.model')
/*const trainerUser = require('../models/trauser.model')
const organizationUser = require('../models/org.model')
const demotransactionModel = require('../models/demotransaction.model')
const demographicsModel = require('../models/demographics.model')*/
const jwt = require('jsonwebtoken')
const config = require('../config')
const httpStatus = require('http-status')
const uuidv1 = require('uuid/v1')
const APIError = require('../utils/APIError')
const log = require('log-to-file');
const waitFor = (ms) => new Promise(r => setTimeout(r, ms));

exports.register = async (req, res, next) => {
  try {
	  
	  
    const activationKey = uuidv1()
    const body = req.body
    body.activationKey = activationKey
    const user = new User(body)
    const savedUser = await user.save()
	
	//log(JSON.stringify(body),'postUserResgister.log');
	
    res.status(httpStatus.CREATED)
    res.send(savedUser.transform())
	
	/*const savedUserDetails = savedUser.transform();
	const savedUserId = savedUserDetails.id;
	
	if(req.body.role == 'user'){
		const trabody = {};
		trabody.mailId = req.body.email;
		trabody.userId = savedUserId;
		trabody.firstName = req.body.name;
		trabody.LastName = req.body.name;
		trabody.mobileNo = req.body.mobile;
		const traUser = new trainerUser(trabody)
		const traSavedUser = await traUser.save()
		
		res.status(httpStatus.CREATED)
		res.send(traSavedUser.transform())	
	}else if(req.body.role == 'admin'){
		
		const orgbody = {};
		orgbody.conPerEmail = req.body.email;
		orgbody.userId = savedUserId;
		orgbody.orgName = req.body.name;
		const orgUser = new organizationUser(orgbody)
		const orgSavedUser = await orgUser.save()
		
		res.status(httpStatus.CREATED)
		res.send(orgSavedUser.transform())
	}*/
	
  } catch (error) {
    return next(User.checkDuplicateEmailError(error))
  }
}

exports.login = async (req, res, next) => {
  try {
    const user = await User.findAndGenerateToken(req.body)
    const payload = {sub: user.id}
    const token = jwt.sign(payload, config.secret)
    return res.json({ message: 'OK', token: token })
  } catch (error) {
    next(error)
  }
}

exports.confirm = async (req, res, next) => {
  try {
	  
    await User.findOneAndUpdate(
      { 'activationKey': req.query.key },
      { 'active': true }
    )
    return res.json({ message: 'OK' , status:200})
  } catch (error) {
    next(error)
  }
}

exports.updateUser = async (req, res, next) => {
  try {
	  const body = req.body
	    // Find the document that describes "legos"
		const query = { '_id': req.params.userId };
		// Set some fields in that document
		const update = {
		  "$set": body
		};
    const updateUserData = await traUser.findOneAndUpdate(query, update)
	if (!updateUserData) throw new APIError(`Unable to Updated the records`, httpStatus.NOT_FOUND)
    return res.json({ message: 'OK' })
  } catch (error) {
    next(error)
  }
}

exports.createDemoTransaction = async (req, res, next) => {
  try {
    const body = req.body
	log(JSON.stringify(req.body),'createDemoTransaction');

	if(body._id && body._id != ""){
	    // Find the document that describes "_id"
		//var uid = mongoose.Types.ObjectId(body._id);
		const query = { '_id': body._id };
		// Set some fields in that document
		delete body['_id'];
		const update = {
		  "$set": body
		};
		const options = { upsert: false, setDefaultsOnInsert: true, strict: false, new: false, runValidators: false };
		const updateDemograhics = await demographicsModel.findOneAndUpdate(query, update , options)
		if (!updateDemograhics) throw new APIError(`Unable to Updated the records`, httpStatus.NOT_FOUND)
		return res.json({ message: 'UPDATED' })
	}else{
		var insertData = [];
		const surveyID = body.surveyId;
		const userId = body.userId;
		const demographic = body.demographic;
		await asyncForEach(body.demographic, async (data, index, arr) => {
		//demographic.forEach(data => { 
			await waitFor(50);
			var tempData = {};
			const demoValuesDetails = await demographicsModel.findDemographicsValues(data.demographicId , data.demographicValues);
			tempData.surveyId = surveyID;
			tempData.userId = userId;
			tempData.demographicTransId = data.demographicId;
			var insertValuesData = [];
			log(JSON.stringify(demoValuesDetails),'createDemoTransaction');
			await asyncForEach(demoValuesDetails.demographicValues, async (demo, index, arr) => {
				await waitFor(40);
				var tempValuesData = {};
				tempValuesData.value = demo.value;
				tempValuesData.demographicValueId = demo._id;
				insertValuesData.push(tempValuesData); 
			});
			tempData.demographicTransValues = insertValuesData;
			insertData.push(tempData); 
		 });
		log(JSON.stringify(insertData),'createDemoTransaction');
		const demoTransactionMultipleSave = await demotransactionModel.insertMany(insertData,{ordered: false}).catch(err=>{
			throw new APIError(err, httpStatus.NOT_FOUND)
		})

		res.status(httpStatus.OK)
		return res.json({ message: 'CREATED' })
	}
	
  } catch (error) {
    return next(demotransactionModel.checkDuplicateDemoTransactionNameError(error))
  }
}

exports.createDemographics = async (req, res, next) => {
  try {
	//const seqName = 'demographicId';
    const body = req.body
	
	
	log(JSON.stringify(req.body),'createDemographics');

	if(body._id && body._id != ""){
	    // Find the document that describes "_id"
		//var uid = mongoose.Types.ObjectId(body._id);
		const query = { '_id': body._id };
		// Set some fields in that document
		delete body['_id'];
		const update = {
		  "$set": body
		};
		const options = { upsert: false, setDefaultsOnInsert: true, strict: false, new: false, runValidators: false };
		const updateDemograhics = await demographicsModel.findOneAndUpdate(query, update , options)
		if (!updateDemograhics) throw new APIError(`Unable to Updated the records`, httpStatus.NOT_FOUND)
		return res.json({ message: 'UPDATED' })
	}else{
		//body._id = await getNextSequenceValue(seqName)
		await waitFor(50);
			//res.send(req.body)
		const demographics = new demographicsModel(body)
		const savedDemographics = await demographics.save()
	
		const savedDemographicsDetails = savedDemographics.transform();
	
		res.status(httpStatus.CREATED)
		res.send(savedDemographicsDetails)

	}
	
  } catch (error) {
    return next(demographicsModel.checkDuplicateDemographicNameError(error))
  }
}

async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);	
  }
}
