const nodemailer = require('nodemailer')
const config = require('../config')

const transporter = {
  host: config.transporter.service,
  port: 465,
  secure: true,
  auth: {
    user: config.transporter.email,
    pass: config.transporter.password
  }
};

module.exports = transporter
